<?php

/**
 * The config file is optional. It accepts a return array with config options
 * Note: Never include more than one return statement, all options go within this single return array
 * In this example, we set debugging to true, so that errors are displayed onscreen. 
 * This setting must be set to false in production.
 * All config options: https://getkirby.com/docs/reference/system/options
 */
return [
    'debug' => true,
];

c::set('site.fontawesome.stylesheet',  'https://use.fontawesome.com/releases/v5.3.0/css/all.css');

c::set('site.jquery.javascript',  'https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js');
c::set('site.slick.javascript',  'assets/js/vendor/slick.min.js');
c::set('site.main.javascript',  'assets/js/main.js');