
<!doctype html>
<html lang="en">
<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width,initial-scale=1.0">
  <title><?= $site->title() ?> | <?= $page->title() ?></title>

  <?= css(['assets/css/index.css', '@auto']) ?>
  <?= css(['assets/css/global.css', '@auto']) ?>

  <?= js(['https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js', '@auto']) ?>
  <?= js(['assets/js/vendor/slick.min.js', '@auto']) ?>
  <?= js(['assets/js/main.js', '@auto']) ?>

</head>
<body>

  <div class="page">
      <a class="logo" href="<?= $site->url() ?>"><?= $site->title() ?></a>

      <nav id="menu" class="menu">
        <?php 
        foreach ($site->children()->listed() as $item): ?>
        <?= $item->title()->link() ?>
        <?php endforeach ?>
      </nav>
    </header>

