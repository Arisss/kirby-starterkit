const { src, series, dest, watch} = require('gulp');
const scss = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const sourcemaps = require('gulp-sourcemaps');

function css() {
  return src('assets/scss/*.scss')
    .pipe(sourcemaps.init())
    .pipe(scss())
    .pipe(autoprefixer())
    .pipe(sourcemaps.write())
    .pipe(dest('assets/css'));
}

watch('assets/scss/**/*.scss', series(css));

exports.default = css;
