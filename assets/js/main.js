$(document).ready(function () {

  var dummy = (function dummy($) {
    var $component = $("body").find('.dummy-component');

    function attachEvents() {

    }

    function init() {
      if (!$component.length) return;
      attachEvents();
    }
    return {
      init: init
    };

  }(jQuery));
  dummy.init();
});


